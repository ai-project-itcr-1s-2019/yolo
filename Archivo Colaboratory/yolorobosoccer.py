# -*- coding: utf-8 -*-
"""yoloRoboSoccer.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/17L2QF26CTbctTc4f6dWeOh4mKosCkozk
"""

# This cell imports the drive library and mounts your Google Drive as a VM local drive. You can access to your Drive files 
# using this path "/content/gdrive/My Drive/"

from google.colab import drive
drive.mount('/content/gdrive')

# Not Necessary cell
# List the content of your local computer folder 
!ls -la "/content/gdrive/My Drive/darknet"

# This cell can be commented once you checked the current CUDA version
# CUDA: Let's check that Nvidia CUDA is already pre-installed and which version is it. In some time from now maybe you 
!/usr/local/cuda/bin/nvcc --version

# We're unzipping the cuDNN files from your Drive folder directly to the VM CUDA folders
!tar -xzvf gdrive/My\ Drive/darknet/cuDNN/cudnn-10.0-linux-x64-v7.5.0.56.tgz -C /usr/local/
!chmod a+r /usr/local/cuda/include/cudnn.h

# Now we check the version we already installed. Can comment this line on future runs
!cat /usr/local/cuda/include/cudnn.h | grep CUDNN_MAJOR -A 2


# Leave this code uncommented on the very first run of your notebook or if you ever need to recompile darknet again.
# Comment this code on the future runs.
!git clone https://github.com/kriyeng/darknet/
# %cd darknet

# Check the folder
!ls

# I have a branch where I have done the changes commented above
!git checkout feature/google-colab

#Compile Darknet
!make

#Copies the Darknet compiled version to Google drive
!cp ./darknet /content/gdrive/My\ Drive/darknet/bin/darknet

# Uncomment after the first run, when you have a copy of compiled darkent in your Google Drive

# Makes a dir for darknet and move there
!mkdir darknet
# %cd darknet

# Copy the Darkent compiled version to the VM local drive
!cp /content/gdrive/My\ Drive/darknet/bin/darknet ./darknet

# Set execution permissions to Darknet
!chmod +x ./darknet


#download files

def imShow(path):

  import cv2

  import matplotlib.pyplot as plt

#   %matplotlib inline

  image = cv2.imread(path)

  height, width = image.shape[:2]

  resized_image = cv2.resize(image,(3*width, 3*height), interpolation = cv2.INTER_CUBIC)

  fig = plt.gcf()

  fig.set_size_inches(18, 10)

  plt.axis("off")

  plt.rcParams['figure.figsize'] = [10, 5]

  plt.imshow(cv2.cvtColor(resized_image, cv2.COLOR_BGR2RGB))

  plt.show()

  

  

def upload():

  from google.colab import files

  uploaded = files.upload() 

  for name, data in uploaded.items():

    with open(name, 'wb') as f:

      f.write(data)

      print ('saved file', name)

def download(path):

  from google.colab import files

  files.download(path)

# Not necessary cell
# Get yolov3 weights
#!wget https://pjreddie.com/media/files/yolov3.weights
  
# Not necessary cell
# Execute darknet using YOLOv3 model with pre-trained weights to detect objects on 'person.jpg'
#!./darknet detector test "/content/gdrive/My Drive/darknet/robosoccer-obj.data" "/content/gdrive/My Drive/darknet/cfg/robosoccer.cfg" "/content/gdrive/My Drive/darknet/backup/robosoccer_last.weights" "/content/gdrive/My Drive/darknet/data/1.jpg" -dont-show

# Show the result using the helper imgShow()
#imShow('predictions.jpg')

#!./darknet detector test "/content/gdrive/My Drive/darknet/robosoccer-obj.data" "/content/gdrive/My Drive/darknet/cfg/robosoccer.cfg" "/content/gdrive/My Drive/darknet/backup/robosoccer_last.weights" "/content/gdrive/My Drive/darknet/data/2.jpg" -dont-show

#imShow('predictions.jpg')

#!./darknet detector test "/content/gdrive/My Drive/darknet/robosoccer-obj.data" "/content/gdrive/My Drive/darknet/cfg/robosoccer.cfg" "/content/gdrive/My Drive/darknet/backup/robosoccer_last.weights" "/content/gdrive/My Drive/darknet/data/3.jpg" -dont-show

#imShow('predictions.jpg')

#!./darknet detector test "/content/gdrive/My Drive/darknet/robosoccer-obj.data" "/content/gdrive/My Drive/darknet/cfg/robosoccer.cfg" "/content/gdrive/My Drive/darknet/backup/robosoccer_last.weights" "/content/gdrive/My Drive/darknet/data/4.jpg" -dont-show

#imShow('predictions.jpg')

#!./darknet detector test "/content/gdrive/My Drive/darknet/robosoccer-obj.data" "/content/gdrive/My Drive/darknet/cfg/robosoccer.cfg" "/content/gdrive/My Drive/darknet/backup/robosoccer_last.weights" "/content/gdrive/My Drive/darknet/data/5.jpg" -dont-show

#imShow('predictions.jpg')

#!./darknet detector test "/content/gdrive/My Drive/darknet/robosoccer-obj.data" "/content/gdrive/My Drive/darknet/cfg/robosoccer.cfg" "/content/gdrive/My Drive/darknet/backup/robosoccer_last.weights" "/content/gdrive/My Drive/darknet/data/6.jpg" -dont-show

#imShow('predictions.jpg')

#!./darknet detector test "/content/gdrive/My Drive/darknet/robosoccer-obj.data" "/content/gdrive/My Drive/darknet/cfg/robosoccer.cfg" "/content/gdrive/My Drive/darknet/backup/robosoccer_last.weights" "/content/gdrive/My Drive/darknet/data/7.jpg" -dont-show

#imShow('predictions.jpg')

#!./darknet detector test "/content/gdrive/My Drive/darknet/robosoccer-obj.data" "/content/gdrive/My Drive/darknet/cfg/robosoccer.cfg" "/content/gdrive/My Drive/darknet/backup/robosoccer_last.weights" "/content/gdrive/My Drive/darknet/data/8.jpg" -dont-show

#imShow('predictions.jpg')

#!./darknet detector test "/content/gdrive/My Drive/darknet/robosoccer-obj.data" "/content/gdrive/My Drive/darknet/cfg/robosoccer.cfg" "/content/gdrive/My Drive/darknet/backup/robosoccer_last.weights" "/content/gdrive/My Drive/darknet/data/9.jpg" -dont-show

#imShow('predictions.jpg')

#!./darknet detector test "/content/gdrive/My Drive/darknet/robosoccer-obj.data" "/content/gdrive/My Drive/darknet/cfg/robosoccer.cfg" "/content/gdrive/My Drive/darknet/backup/robosoccer_last.weights" "/content/gdrive/My Drive/darknet/data/152.jpg" -dont-show

#imShow('predictions.jpg')

#!./darknet detector test "/content/gdrive/My Drive/darknet/robosoccer-obj.data" "/content/gdrive/My Drive/darknet/cfg/robosoccer.cfg" "/content/gdrive/My Drive/darknet/backup/robosoccer_last.weights" "/content/gdrive/My Drive/darknet/data/155.jpg" -dont-show

#imShow('predictions.jpg')

#!./darknet detector test "/content/gdrive/My Drive/darknet/robosoccer-obj.data" "/content/gdrive/My Drive/darknet/cfg/robosoccer.cfg" "/content/gdrive/My Drive/darknet/backup/robosoccer_last.weights" "/content/gdrive/My Drive/darknet/data/160.jpg" -dont-show

#imShow('predictions.jpg')

#!./darknet detector test "/content/gdrive/My Drive/darknet/robosoccer-obj.data" "/content/gdrive/My Drive/darknet/cfg/robosoccer.cfg" "/content/gdrive/My Drive/darknet/backup/robosoccer_last.weights" "/content/gdrive/My Drive/darknet/data/167.jpg" -dont-show

#imShow('predictions.jpg')

#!./darknet detector test "/content/gdrive/My Drive/darknet/robosoccer-obj.data" "/content/gdrive/My Drive/darknet/cfg/robosoccer.cfg" "/content/gdrive/My Drive/darknet/backup/robosoccer_last.weights" "/content/gdrive/My Drive/darknet/data/m1.jpg" -dont-show

#imShow('predictions.jpg')

#!./darknet detector test "/content/gdrive/My Drive/darknet/robosoccer-obj.data" "/content/gdrive/My Drive/darknet/cfg/robosoccer.cfg" "/content/gdrive/My Drive/darknet/backup/robosoccer_last.weights" "/content/gdrive/My Drive/darknet/data/lo1.jpeg" -dont-show

#imShow('predictions.jpg')

#!./darknet detector test "/content/gdrive/My Drive/darknet/robosoccer-obj.data" "/content/gdrive/My Drive/darknet/cfg/robosoccer.cfg" "/content/gdrive/My Drive/darknet/backup/robosoccer_last.weights" "/content/gdrive/My Drive/darknet/data/lo2.jpeg" -dont-show

#imShow('predictions.jpg')



# Copy fils from Google Drive to the VM local filesystem
!cp -r "/content/gdrive/My Drive/darknet/data/obj" ./img

!./darknet detector train "/content/gdrive/My Drive/darknet/robosoccer-obj.data" "/content/gdrive/My Drive/darknet/cfg/robosoccer.cfg" "/content/gdrive/My Drive/darknet/backup/robosoccer_lastt4.weights" -dont_show

