#############################################
# Object detection - YOLO - OpenCV
# Author : Arun Ponnusamy   (July 16, 2018)
# Website : http://www.arunponnusamy.com
############################################


import argparse
import numpy as np
import cv2
import time
import requests
import urllib
import random
import socket,pickle
from threading import Thread

serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
clientSocket = None

IZQ = 1
CENTRO = 0
DERECHA = 2
IS_BALL = True
# **************************************

#   SOBRE MOVIMIENTOS Y POSICIONES

# 1 = izquierda, 0 = centro , 2 = derecha

#***************************************

left=1
right = 2
center=0

configFile = "robosoccerv3-tiny-Testing.cfg"
weigthsFile = "robosoccerv3-tiny_final.weights"
classesFile =  "classesNames.txt"
inpWidth = 416
inpHeight = 416

#nivel de confianza para aceptar una detección como correcta
confidenceThreshold = 0.60

xCenter= inpWidth // 2
yCenter = inpHeight // 2
xCenterThreshold = round(inpWidth * 0.10)

#permite que se dejen pasar hasta una cantidad de detecciones sin encontrar la bola, de lo contrario se inicia a buscarla
forgivenessAmount = 2

ballForgiveness = forgivenessAmount

#si no se encuentra la bola se iniciará a girar hacia la última posición en la que se vió la bola.
lastBallDirection=0 # el ultimo elemento representa la dirección en la que estaba la bola la última vez que la vió el robot

ballPositionAndConf= [0,0,0,0,0] #valores x y w h confianza de la detección
markPositionAndConf=[0,0,0,0,0]  #valores x y w h confianza de la detección
ballFound=False
markFound=False
tennisBallClass = "tennisBall"
markClass = "mark"


url = "http://192.168.43.252:8080/shot.jpg"
PORT = 9000
BUFFER_SIZE = 1024

conf_threshold = 0.7
nms_threshold = 0.4
TIMES_BACKWARD = 1



classes = None

with open(classesFile, 'r') as f:
    classes = [line.strip() for line in f.readlines()]

COLORS = np.random.uniform(0, 255, size=(len(classes), 3))
    
def get_output_layers(net):
    
    layer_names = net.getLayerNames()
    
    output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]

    return output_layers


# ********************************************************************************

# Para determinar los movimientos del robot

# ********************************************************************************


#determina si la bola se encuentra a la izquierda, derecha o frente al robot
def getBallPosition(ballPosition):

    
    xTemp = ballPosition[0] + ballPosition[2]
    xDistance  = (xTemp - ballPosition[0])

    #centro del bounding box
    boundingBoxCenter = xTemp - (xDistance // 2)

    print("bounding center ", boundingBoxCenter)
    print("threshlod ", xCenterThreshold)
    print("xCenter ", xCenter)
    print(xCenter - xCenterThreshold)
    print(xCenter + xCenterThreshold)
    
    # si la bola se encuentra en el centro
    if boundingBoxCenter >= (xCenter - xCenterThreshold) and boundingBoxCenter <= (xCenter + xCenterThreshold):

        return center
    
    elif boundingBoxCenter < (xCenter - xCenterThreshold):

        return left

    else:

        return right



def getRobotSpinDirection(lastDirection):

    # si la última vez la bola estuvo frente al robot
    
    if lastDirection == center:

        #girar en una direccion aleatoria
        return random.randrange(left, right + 2)
        
    else:

        return lastDirection;

def getRobotMovement():

    global IS_BALL
    global ballForgiveness
    global ballFound
    global markFound
    global ballPositionAndConf
    global lastBallDirection
    global clientSocket
  
    # si no se detecta la bola y ya se cumplió con la cantidad de veces que se permite no detectarla antes de buscarla
    if ballFound == False:

        if ballForgiveness == 0:

            print("************************************")
            print("")
            print("Se debe buscar la bola, determinando giro")
            
            spinDirection = getRobotSpinDirection(lastBallDirection)
            if(IS_BALL):
                print("hacia atrás...", spinDirection)
                print("")
                print("************************************")
                clientSocket.send(str(3).encode())
                IS_BALL = False

            clientSocket.send(str(spinDirection).encode())

            print("Girar hacia la dirección  ", spinDirection)
            print("")
            print("************************************")
            
            ballForgiveness = forgivenessAmount;

        else:

            ballForgiveness -= 1

            print("************************************")
            print("Perdonando, quedan ",ballForgiveness, " más")
            print("************************************")

    else:
        IS_BALL = True
        res = getBallPosition(ballPositionAndConf)

        #actualizando la última dirección donde se vió la bola
        lastBallDirection =res

        print(" !!!!!!!!!!!!!!!!!!!!!!!! BALL POSITION ", res , " !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")

        # si se encuentra la bola y la marca se debe patear con fuerza el balón
        if markFound == True and res == center:
            clientSocket.send(str(res).encode())
            print("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
            print("")
            print("                  PATEAR AHORA           ")
            print("")
            print("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
            

        # si se encuentra la bola pero no está la marca (logo avengers) moverse hacia la bola pero
        # con una velocidad más reducida si la bola se encuentra en el centro
        else:

            clientSocket.send(str(res).encode())
            print ("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&")
            print("")
            print("                    DIRIGIRSE DESPACIO HACIA LA BOLA   DIRECCION ", res)
            
            print("")
            print ("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&")

        
        
    
    

def assignCoordinatesArray(coordsArray, detectionCoords):

     coordsArray [0] = detectionCoords[0]
     coordsArray [1] = detectionCoords[1]
     coordsArray [2] = detectionCoords[2]
     coordsArray [3] = detectionCoords[3]


def manageDetectionForMovements(objectDetectedArray,objectDetectedFlag ,className,classID,confidence,boundingBoxCoords):

    if classes[classID] == className:

        
        if objectDetectedFlag == True:

            #si existe una detección mejor de un objeto que ya se había detectado
            if objectDetectedArray[4] < confidence:
                
                assignCoordinatesArray(objectDetectedArray,boundingBoxCoords)
                objectDetectedArray[4] = confidence

        else:
            assignCoordinatesArray(objectDetectedArray,boundingBoxCoords)
            
            

        return True

    else:
        return False
            
    


def draw_prediction(img, class_id, confidence, x, y, x_plus_w, y_plus_h):

    label = str(classes[class_id])

    color = COLORS[class_id]
  

    cv2.rectangle(img, (x,y), (x_plus_w,y_plus_h), color, 2)

    cv2.putText(img, label, (x-10,y-10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)


def manageImageGeneration(image,boxes,class_ids,confidences,):

    
    
    indices = cv2.dnn.NMSBoxes(boxes, confidences, conf_threshold, nms_threshold)

    for i in indices:
        i = i[0]
        box = boxes[i]
        x = box[0]
        y = box[1]
        w = box[2]
        h = box[3]
        draw_prediction(image, class_ids[i], confidences[i], round(x), round(y), round(x+w), round(y+h))

    cv2.imwrite("object-detection.jpg", image)



def manageObjectDetections(image,outPut,width,height):

    
    class_ids = []
    confidences = []
    boxes = []
    
    global ballFound
    global markFound
    global ballPositionAndConf
    global markPositionAndConf
    
    ballFound=False
    markFound=False
    ballPositionAndConf = [0,0,0,0,0]
    markPositionAndConf = [0,0,0,0,0]
    

    for out in outPut:
        
        for detection in out:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]

            # si el objeto detectado tiene un grado de confianza mayor a 0.70
            if confidence > confidenceThreshold:
                
                center_x = int(detection[0] * width)
                center_y = int(detection[1] * height)
                w = int(detection[2] * width)
                h = int(detection[3] * height)
                x = center_x - w / 2
                y = center_y - h / 2
                class_ids.append(class_id)
                ballRes = manageDetectionForMovements(ballPositionAndConf,ballFound,tennisBallClass,class_id,confidence,[x,y,w,h])
                ballFound = ballRes
                #si la detección no contenía la bola
                if ballRes == False:
                    
                    markRes = manageDetectionForMovements(markPositionAndConf,markFound,markClass,class_id,confidence,[x,y,w,h])
                    markFound = markRes
                    
                confidences.append(float(confidence))
                boxes.append([x, y, w, h])

    
    getRobotMovement()
    
    #manageImageGeneration(image,boxes,class_ids,confidences)

    
def makePrediction(image):

    
    #width = image.shape[1]
    #height = image.shape[0]
    width = inpWidth
    height = inpHeight
    scale = 0.00392
    

    net = cv2.dnn.readNet(weigthsFile, configFile)
    
    blob = cv2.dnn.blobFromImage(image, scale, (inpWidth,inpHeight), (0,0,0), True, crop=False)

    #para que el tamaño de la imagen a generar sea como lo indican las variables inpWidth y inpHeight
    resized = cv2.resize(image, (inpWidth,inpHeight), interpolation = cv2.INTER_AREA) 

    net.setInput(blob)

    outs = net.forward(get_output_layers(net))

    manageObjectDetections(resized,outs,width,height)

def sendToClient(msg, clientSocket):
    clientSocket.send("Connection!".encode() )

def handleClient(clientSocketLocal):
    acks = []
    clientSocket = clientSocketLocal
    while True:

        data = clientSocket.recv(BUFFER_SIZE)
        msg = data#pickle.loads(data)
        if msg != bytes("exit", "utf8"):
            
            if(msg == "ACK"):
                break
            else:
                sendToClient(msg, clientSocket)
                break

        else:
            clientSocket.sendToClient("Connection closed!".encode() )
            clientSocket.close()
            break

def acceptIncomingConnections():
    global clientSocket
    while True:
        clientSocketL, client_address = serverSocket.accept()
        print("%s:%s has connected." % client_address)
        print(serverSocket)
        #captureImages()
        clientSocket = clientSocketL
        #clientSocket.send(str("init message!!").encode())
        Thread(target=captureImages).start()

def initServer():
    serverSocket.bind(("", 9000))
    print(serverSocket)
    serverSocket.listen(1)
    print("Waiting for connections...")
    ACCEPT_THREAD = Thread(target=acceptIncomingConnections)
    ACCEPT_THREAD.start()
    ACCEPT_THREAD.join()
    print("max connections reached!")
    serverSocket.close()

def captureImages():
    
    while cv2.waitKey(1) < 0:
        try:
        
            imgResp = urllib.request.urlopen(url)
            imgNp = np.array(bytearray(imgResp.read()),dtype=np.uint8)
            img1=cv2.imdecode(imgNp,-1)
            
        
            makePrediction(img1)
                
                
        except Exception as e:
            print(e)
            continue
initServer()
